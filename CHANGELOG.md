Change Log
==========

0.4.2: 2025/01/32
------------------

Fix issue with working directory containing space character(s)

0.4.0: 2024/11/13
------------------

Add 'get_partition_walltime_infos' function

0.3.3: 2023/11/16
------------------

Make SBatchScriptJob handling request for a specific gpu

0.3.2: 2023/11/16
------------------

Add 'get_partition_gpus' function.

0.3.0: 2023/11/09
------------------

Allow user to define module to load before executing the script

0.2.3 : 2023/06/27
------------------

fix pycuda cache dir: make sure it is unique per script

0.2.2 : 2023/03/29
------------------

utils: add get_partitions function

0.2.1 : 2023/03/15
------------------

SBatchScriptJob: add `collect_logs` function

0.2.0 : 2023/01/20
------------------

add 'timeout' parameter to 'submit' function

0.1.0 : 2023/01/20
------------------

initial release
