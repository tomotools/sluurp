from .executor import submit  # noqa F401

__version__ = "0.4.2"
