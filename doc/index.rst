sluurp
======


Getting started
---------------

Install requirements

.. code:: bash

    python3 -m pip install sluurp

Run the tests

.. code:: bash

    python3 -m pip install sluurp[test]
    python3 -m pytest --pyargs sluurp.tests

Documentation
-------------

.. toctree::
    :maxdepth: 2

    tutorials/index
    api
